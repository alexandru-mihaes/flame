import React from 'react';
import "./styles/index.css";
import "./styles/color-palette.css";
import "./styles/font-size.css";

import Header from './modules/components/organs/header/Header'
import Sidebar from './modules/components/organs/sidebar/Sidebar'
import Body from './modules/components/organs/body/Body'

import * as S from './App.style';

function App() {
  return (
    <S.App>
      <Header />
      <S.ContentWrapper>
        <S.Content>
          <Sidebar />
          <Body />
        </S.Content>
      </S.ContentWrapper>
    </S.App>
  );
}

export default App;

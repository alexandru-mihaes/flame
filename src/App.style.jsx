import styled from 'styled-components';

export const App = styled.div`
  display: grid;
  grid-template-areas: "header  header"
                       "content content";
`;

export const ContentWrapper = styled.div`
  grid-area: content;
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const Content = styled.div`
  width: 1140px;
  display: grid;
  grid-template-areas: "sidebar body";
    grid-template-columns: 1fr 3fr;
`;
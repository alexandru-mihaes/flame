import React from 'react';

import Title from './../components/molecules/body/title/Title';
import Subtitle from './../components/molecules/body/subtitle/Subtitle';

import * as S from './../components/molecules/body/styles/Components.style';

const Item2 = () => (
  <React.Fragment>
    <Title name="Item 2"/>
    <S.Content>
      <Subtitle name="Subtitle"/>
      <S.Table>
        <thead>
          <tr>
            <th>Cell</th>
            <th>Cell</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Cell</td>
            <td>Cell</td>
          </tr>
          <tr>
            <td>Cell</td>
            <td>Cell</td>
          </tr>
        </tbody>
      </S.Table>
      <br/>
    </S.Content>
  </React.Fragment>
);

export default Item2;
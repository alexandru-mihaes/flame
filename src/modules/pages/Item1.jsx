import React from 'react';

import Title from './../components/molecules/body/title/Title';
import Subtitle from './../components/molecules/body/subtitle/Subtitle';

import * as S from './../components/molecules/body/styles/Components.style';

const Item1 = () => (
  <React.Fragment>
    <Title name="Item 1"/>
    <S.Content>
      <Subtitle name="Subtitle"/>
      <S.Chapter>
        <S.Line>Line with <S.Highlight color="flame">HIGHLIGHTED</S.Highlight> word</S.Line>
      </S.Chapter>
    </S.Content>
  </React.Fragment>
);

export default Item1;
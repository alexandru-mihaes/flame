import styled from 'styled-components';
import { NavLink } from 'react-router-dom'

export const Item = styled(NavLink)`
  font-size: var(--text-medium-small);
  text-decoration: none;
  color: var(--graphite);

  :hover {
    text-decoration: underline;
  }

  &.active {
    font-weight: 500;
    color: var(--flame);
  }
`;
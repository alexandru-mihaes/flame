import React from 'react';

import * as S from './Item.style';

const Item = (props) => {
  return (
    <div>
      <S.Item
        exact = {props.exact}
        to = {props.to}>
          {props.name}
      </S.Item>
    </div>
  );
}

export default Item;
import styled from 'styled-components';

export const Subtitle = styled.p`
  margin: 0;
  font-size: var(--text-larger);
  font-weight: 700;
  color: var(--primary-light);
`;
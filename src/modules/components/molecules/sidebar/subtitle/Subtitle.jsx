import React from 'react';

import * as S from './Subtitle.style';

const Subtitle = (props) => {
  return (
    <div>
      <S.Subtitle>
        {props.name}
      </S.Subtitle>
    </div>
  );
}

export default Subtitle;
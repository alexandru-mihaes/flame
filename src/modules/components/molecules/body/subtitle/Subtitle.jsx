import React from 'react';

import * as S from './Subtitle.style';

const Subtitle = (props) => {
  return (
    <S.Subtitle>
      {props.name}
    </S.Subtitle>
  );
}

export default Subtitle;
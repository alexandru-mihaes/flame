import styled from 'styled-components';

export const Subtitle = styled.p`
  width: fit-content;
  margin: 30px 0px 20px;
  font-size: var(--text-larger);
  font-family: 'Zilla Slab';
  font-weight: 600;
  color: var(--primary-light);
`;
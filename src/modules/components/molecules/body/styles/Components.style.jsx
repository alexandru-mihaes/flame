import styled from 'styled-components';

export const Content = styled.p`
  font-size: var(--text-medium);
  color: var(--graphite);
`;

export const Chapter = styled.p`
  font-size: var(--text-medium);
  color: var(--graphite);
`;

export const Line = styled.p`
  display: block;
  margin: 0 0 12px 0;

  * > {
    display: inline-block;
  }
`

export const Highlight = styled.p`
  display: inline-block;
  margin: 0;
  color: ${props => props.color === 'flame' ? 'var(--flame)'
                                            : 'var(--graphite)'};
`

export const Table = styled.table`
  border-collapse: collapse;
  margin: 25px 0;
  font-size: 0.9em;
  min-width: 400px;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0,0,0, 0.15);
  
  thead tr {
    font-family: 'Zilla Slab';
    background-color: var(--flame);
    color:var(--light);
    text-align: left;
    font-weight: bold;
    text-transform: capitalize;
  }

  th, td {
    text-align: center;
    padding: 12px 15px;
  }

  tbody tr {
    color: var(--graphite);
  }

  tbody tr:nth-of-type(even){
    background-color: var(--flame-lighter);
  }

  tbody tr:nth-of-type(odd) {
    background-color: var(--flame-lightest);
  }

  tbody tr:first-of-type {
    border-top: 2px solid var(--flame);
  }

  tbody tr:last-of-type{
    border-bottom: 2px solid var(--flame);
  }

  tbody tr.active__row{
    font-weight: bold;
    color: #f1c40f;
  }
`;
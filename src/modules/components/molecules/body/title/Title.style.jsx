import styled from 'styled-components';

export const Title = styled.p`
  position: relative;
  width: fit-content;
  margin: 0;
  padding-bottom: 5px;
  font-size: var(--text-extra-large);
  font-family: 'Zilla Slab';
  font-weight: 600;
  color: var(--primary-light);

  :before {
    position: absolute;
    content: "";
    bottom: 0;
    left: 0;
    width: 30%;
    height: 3px;
    background: var(--flame);
  }
`;
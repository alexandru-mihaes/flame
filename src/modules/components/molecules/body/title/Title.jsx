import React from 'react';

import * as S from './Title.style';

const Title = (props) => {
  return (
    <S.Title>
      {props.name}
    </S.Title>
  );
}

export default Title;
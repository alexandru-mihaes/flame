import React from 'react';
import { version } from '../../../../../package.json';

import * as S from './Logo.style';

const Logo = () => {
  return (
    <React.Fragment>
        <S.Logo
          to = "/home"/>
        <S.Version
          href = "https://bitbucket.org/axrmhs/flame/src/master/">
            {version}
        </S.Version>
    </React.Fragment>
  );
}

export default Logo;
import styled from 'styled-components';
import { Link } from 'react-router-dom'

import logoAsset from '../../../../assets/logo.png'

export const Logo = styled(Link)`
  display: inline-block;
  align-self: center;
  width: auto;
  height: 50px;
  background-size: contain;
  background-image: url(${logoAsset});
  background-repeat: no-repeat;
  text-decoration: none;
  text-transform: uppercase;
  font-size: var(--text-larger);
  font-weight: 450;
  color: var(--light);

  :after {
      vertical-align: -5%;
      margin-left: 50px;
      content: "Flame";
  }
`;

export const Version = styled.a`
  align-self: center;
  padding-top: 8px;
  padding-left: 10px;
  text-decoration: none;
  font-size: var(--text-small);
  color: var(--light);
  transition: 0.1s ease-in;

  :hover {
    text-decoration: underline;
    color: var(--cyan);
  }
`

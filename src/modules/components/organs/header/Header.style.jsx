import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  grid-area: header;
  height: 70px;
  width: 100%;
  padding: 12px 0;
  display: flex;
  justify-content: center;
  background-color: var(--primary);
`;

export const Header = styled.div`
  width: 1140px;
  display: flex;
`;
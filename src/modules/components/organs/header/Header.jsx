import React from 'react';
import { withRouter } from "react-router-dom";

import Logo from "../../molecules/logo/Logo.jsx";

import * as S from './Header.style';

const Header = () => (
  <S.HeaderWrapper>
    <S.Header>
      <Logo />
    </S.Header>
  </S.HeaderWrapper>
);

export default withRouter(Header);
  
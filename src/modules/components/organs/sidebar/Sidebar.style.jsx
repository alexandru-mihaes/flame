import styled from 'styled-components';

export const Sidebar = styled.nav`
  grid-area: sidebar;
  margin: 40px 15px 0 12px;
  border-right: 1px solid var(--gray);

  > div {
    margin: 10px 0;
  }
`;
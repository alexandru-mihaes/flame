import React from 'react';

import Subtitle from '../../molecules/sidebar/subtitle/Subtitle'
import Item from '../../molecules/sidebar/item/Item'

import * as S from './Sidebar.style';

const Sidebar = () => (
  <S.Sidebar>
    <Subtitle name="Subtitle"/>
    <Item
      exact
      to="/item1"
      name="Item 1" />
    <Item
      to="/item2"
      name="Item 2" />
  </S.Sidebar>
);

export default Sidebar;
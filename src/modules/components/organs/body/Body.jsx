import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Item1 from '../../../pages/Item1'
import Item2 from '../../../pages/Item2'

import * as S from './Body.style';

const Body = () => (
  <S.Body>
    <Switch>
      <Route
        exact
        path = '/item1'
        component = {Item1} />
      <Route
        path = '/item2'
        component = {Item2} />
    </Switch>
  </S.Body>
);

export default Body;
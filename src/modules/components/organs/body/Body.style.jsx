import styled from 'styled-components';

export const Body = styled.div`
  grid-area: body;
  margin: 40px 0 0 15px;
`;